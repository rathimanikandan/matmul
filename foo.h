#ifndef FOO_DOT_H    /* This is an "include guard" */
#define FOO_DOT_H    /* prevents the file from being included twice. */
                     /* Including a header file twice causes all kinds */
                     /* of interesting problems.*/

/**
 * This is a function declaration.
 * It tells the compiler that the function exists somewhere.
 */
int **MatrixMultiplication(int first_row,int first_col,int second_row,int second_col,int** matrix1,int** matrix2);

#endif /* FOO_DOT_H */